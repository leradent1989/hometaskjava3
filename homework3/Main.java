package homework3;

import  java.util.Scanner;
import java.util.Arrays;


public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        String[][] schedule = new String[7][2];

        String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        String[] tasks = {"brush teeth,drink tea", "go to the cinema,swim in a lake", "go for a walk,cook dinner", "make 5 exercises,record speech", "visit dentist,visit hairstylist", "write summary,bake pie", "go to the gym,write project"};

        int index;
        boolean bool = true;

        for (int i = 0; i < days.length; i++) {

            schedule[i][0] = days[i];
            schedule[i][1] = tasks[i];
        }
        while (bool) {
            System.out.println("Enter day of the week");
            String dayFirst = in.nextLine().toLowerCase().trim();
            if(dayFirst.equals("")){dayFirst = "day";}
          String [] dayArr = dayFirst.split(" ");
             String day =   dayFirst.substring(0,1).toUpperCase() + dayFirst.substring(1);
              if(day.equals("Exit")){
                  day ="exit";
              }
            index = Arrays.asList(days).indexOf(day);

            switch (day) {
                case "Sunday":

                    System.out.println("Your tasks for "+ schedule[index][0]+ ": " + schedule[index][1]);
                    break;
                case "Monday":

                    System.out.println(" Your tasks for 1." + schedule[index][0] + ": " + schedule[index][1]);
                    break;
                case "Tuesday":

                    System.out.println("Your tasks for 2."+ schedule[index][0] +": " + schedule[index][1]);
                    break;
                case "Wednesday":

                    System.out.println("Your tasks for 3." + schedule[index][0] + ": "+ schedule[index][1]);
                    break;
                case "Thursday":

                    System.out.println("Your tasks for 4." + schedule[index][0] + " : " + schedule[index][1]);
                    break;
                case "Friday":

                    System.out.println("Your tasks for 5." + schedule[index][0] + ": " + schedule[index][1]);
                    break;
                case "Saturday":

                    System.out.println("Your tasks for 6." + schedule[index][0] + ": " + schedule[index][1]);
                    break;

                case "exit":
                    bool = false;
                    break;
                default:
                    if ( dayArr.length > 1 && dayArr[0].equals("change") || dayArr[0].equals("reschedule")) {
                        String secondWord = dayArr[1].substring(0,1).toUpperCase() + dayArr[1].substring(1);
                        if ( Arrays.asList(days).contains(secondWord)) {
                            System.out.println("Please enter new task for " + dayArr[1]);
                            index = Arrays.asList(days).indexOf(secondWord);
                            String newTask = in.nextLine();
                            schedule[index][1] = newTask;
                        } else System.out.println("Sorry I don't understand you Please try again");
                    }else  System.out.println("Sorry I don't understand you Please try again");
                    break;
            }
        }

    }
}
